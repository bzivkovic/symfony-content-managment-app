<?php

namespace Yoda\EventBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name, $count)
    {
        // $em = $this->get('doctrine')->getManager();
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('EventBundle:Event');

        $event = $repo->findOneBy(array(
           'name' => "Dart's suprise birthday party!"
        ));

        return $this->render(
            'EventBundle:Default:index.html.twig',
            array('name' => $name, 'count' => $count, 'event' => $event)
        );

//        $data = array(
//            'count' => $count,
//            'firstName' => $name,
//            'ackbar' => "It's a traaaaaap!"
//        );
//        $json = json_encode($data);
//
//        $response = new Response($json);
//        $response->headers->set('Content-Type', 'application/json');
//
//        return $response;
    }
}
