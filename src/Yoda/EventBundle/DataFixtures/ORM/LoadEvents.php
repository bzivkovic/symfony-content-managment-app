<?php

namespace Yoda\EventBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Yoda\EventBundle\Entity\Event;

class LoadEvents implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $event1 = new Event();
        $event1->setName("Dart's suprise birthday party!");
        $event1->setLocation('Deathstar');
        $event1->setTime(new \DateTime('tomorrow noon'));
        $event1->setDetails('Ha! Darth HATES suprises !!!');
        $manager->persist($event1);

        $event2 = new Event();
        $event2->setName("Dart's suprise birthday party!");
        $event2->setLocation('Deathstar');
        $event2->setTime(new \DateTime('tomorrow noon'));
        $event2->setDetails('Ha! Darth HATES suprises !!!');
        $manager->persist($event2);

        $manager->flush();
    }
}